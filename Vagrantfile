# -*- mode: ruby -*-
# vi: set ft=ruby ts=2 sts=2 sw=2 et :

# Load the configuration from the config file
CONFIG_FILE = ENV['HOST_CONFIG'] ||= 'config.yml'
host_config = YAML.load_file(File.expand_path('../' + CONFIG_FILE, __FILE__))

Vagrant.configure("2") do |config|
  # Virtualbox provider configuration and overrides
  config.vm.provider "virtualbox" do |v, override|
    override.vm.box = "centos/7"
  end

  # Docker provider configuration and overrides
  config.vm.provider "docker" do |d|
    d.image = "datapunt/vagrant-docker-ubuntu"
    d.has_ssh = true
    d.remains_running = true
    d.force_host_vm = false
    d.volumes = ["/sys/fs/cgroup:/sys/fs/cgroup:ro"]
    d.privileged = true
  end

  # SSH no worky
  config.ssh.insert_key = false

  # Don't check for updates, it's annoying
  config.vm.box_check_update = false

  # Setup the base ansible dict
  ansible_groups = {}

  # Loop the groups
  host_config['groups'].each_with_index do |(group, members), group_index|
    ansible_groups[group] = []

    # Loop the hosts
    members.each_with_index do |member, member_index|
      ansible_groups[group].push(member['hostname'])

      config.vm.define member['hostname'] do |machine|

        # Setup defaults
        box = member['box'] ||= 'centos/7'
        hostname = member['hostname']
        cpus = member['cpus'] ||= 1
        memory = member['memory'] ||= 512

        machine.vm.hostname = hostname

        # Virtualbox provider configuration
        machine.vm.provider "virtualbox" do |v, override|
          v.cpus = cpus
          override.vm.box = box
          v.memory = memory
        end

        # Setup private networks
        if member['private_ips']
          member['private_ips'].each do |address|
            machine.vm.network "private_network", ip: address
          end
        end

        # Setup public networks
        if member['public_ips']
          member['public_ips'].each do |address|
            machine.vm.network "private_network", ip: address
          end
        end

        # Ansible section (logic for last host of last group)
        if host_config['ansible']
          if host_config['groups'].length == group_index + 1
            if members.length == member_index + 1
              machine.vm.provision 'ansible' do |a|
                a.compatibility_mode = '2.0'
                a.playbook = host_config['ansible']['playbook']
                a.raw_arguments = host_config['ansible']['raw_arguments']
                a.limit = host_config['ansible']['limit']
                a.groups = ansible_groups
                a.tags = ENV['ANSIBLE_TAGS'] ||= host_config['ansible']['tags']
              end
            end
          end
        end
      end
    end
  end
end
