.DEFAULT_GOAL := help
SHELL := /bin/bash
TAG := $(shell git describe --tags --abbrev=0)

##@ Setup
setup: dependencies config.yml build-container ## Check everything needed for first run

dependencies:
	@echo "Checking Vagrant installed..."
	@which vagrant > /dev/null
	@echo "Checking Docker installed..."
	@which docker > /dev/null

config.yml:
	cp examples/demo_cluster.yml config.yml

##@ (Re)Build Docker Container
build-container:	## Build the docker container
	@$(MAKE) -C docker/ubuntu/18.04 build

##@ Vagrant control
.PHONY: up up-docker
up: up-docker		## Alias for up-docker

up-docker:			## Bring up the environment
	@vagrant up --provider docker

destroy:			## Blow it all up
	@vagrant destroy -f && rm -fr .vagrant

provision:			## Provision ansible
	@vagrant provision

##@ Release
.PHONY: release
release:			## Create an archive
	@git archive --format tar.gz --output vagrant-hostconfig-$(TAG).tar.gz HEAD

##@ Helpers
cloc:				## Show Lines of Code analysis
	@cloc --vcs git --quiet

help:				## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
