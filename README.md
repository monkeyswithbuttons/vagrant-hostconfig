# Vagrant-Ansible Boilerplate

A way of making ansible local development easier to manage.

## Requirements

You'll need to have vagrant>=2.2.4, ansible, and docker installed.

## Getting Started
To get started, simply use the provided Makefile

    # Check dependencies
    make setup
    # Build cluster
    make up-docker
    # Run provisioning again
    make provision

## Usage

```bash
  Usage:
    make <target>

  Setup
    setup            Check dependencies and build requirements

  (Re)Build Docker Container
    build-container  Build the docker container

  Vagrant control
    up-docker        Bring up the environment
    destroy          Blow it all up
    provision        Provision ansible

  Release
    release          Create an archive

  Helpers
    cloc             Show Lines of Code analysis
    help             Display this help
```

## Configuration

The Vagrantfile expects a yaml configuration file that contains the following syntax:

```bash
  ansible:
    playbook: provisioning/playbook.yml
  groups:
    <group>:
      - hostname: <hostname>
      - hostname: <hostname>
      - hostname: <hostname>
    <group>:
      - hostname: <hostname>
      - hostname: <hostname>
      - hostname: <hostname>
```

This file location can be changed using the `HOST_CONFIG` variable.  For example:

```bash
HOST_CONFIG=examples/single_node_external_codebase.yml vagrant status
```

Optionally, the `group` can contain other configurations:

* box

  sets the virtualbox box flavor - default generic/centos7

* memory

  sets the virtualbox memory (MB) - default 512

* cpus

  sets the virtualbox number of cpus - default 1

* public_ip

  sets the public ip address of the network interface - default not defined

* private_ip

  sets the private ip address of the network interface - default not defined
